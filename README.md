Babylon Android Tech Test
=========================

![main page](readme/splash.png)

Babylon Tech Test project implementation. Mostly focused on architecture side,
but trying to keep UI in place.

## Background

Implementation is based on my 
[MVP library](https://github.com/imbryk/android-mvp/tree/develop) 
which I started developing some time ago to help reduce boilerplate we 
had in my proposed MVP approach in
[esky project](https://play.google.com/store/apps/details?id=com.esky) 
(this MVP is still used for new features by esky team).

The described MVP model had Presenters and ViewState which survive android 
configuration changes, with the help of _Dagger_ and android's own 
_NonConfigurationInstance_. 
ViewState was an approach of persisting the state of a view - such a state 
would be a POJO class implementing the same View interface but not a real view - 
which could be e.g. an android class (destroyed on config changes).

The library itself uses annotation processor to generate most of the necessary
code to manage MVP components... 
but because of different reasons it never left it's alpha stage.

Meanwhile as kotlin arrived and data binding started working so well, I do not 
se the proposed MVP with ViewState approach as any longer necessary.  
One can simply use persistent ViewModels and bind data to native views 
(although that does has some small downsides to using View interface...).

Even though the lib became obsolete even before it left its alpha stage -
I'm still very happy with the way it was written, so please have a look also ;)

## Implementation
The library with a few small tricks (of course modifying the source of the 
lib itself would be best) can be used with databinding and that is what 
I'm doing in the project. 

Both Presenter and ViewModel are injected and persist screen rotations, 
while ViewModel uses binding to display data in android Views.

I think that I'm using Model-View-Presenter-ViewModel here ;) Let's just say 
I use layered architecture.

### Structure
#### network 
Completely separated package for network calls. Implemented as Retrofit, but 
any custom implementation which fulfills `Api` interfaces would work. The 
data is provided as defined in `NetworkDTOs` 

#### domain 
That's where domain specific functionality happens - fully test driven usecases
and domain models sit in this package. Usecases may use different data sources (network or 
platform specific) - in this case they use only injected network api's - no other 
dependencies should be here.

#### presentation 
Presenter and ViewModels (used for databinding) are here. Another fully testable 
package, with dependency to domain, and a bit to platform (especially in case 
of ViewModels)... further separation of android knowledge might perhaps be 
beneficial... or might be over-engineering - in application that is never 
supposed to work on different platform.  
Platform separation in this package goes as far as needed for testing purposes.

#### android
All the android specific classes are here - `Application` `Activities` `Adapters`.
Classes from here have (indirect) access to presentation package. They should
be agnostic about any domain or network packages.
`Application` and `Activities` are __GOD__ objects - they make everything happen.
In this case they must fire dependency injection, inflation, databinding, 
navigation, and passing data between screens... sounds like a lot - but practice 
shows that even with all those roles they can stay quite lean.  
They are not tested in this project, although once navigation and data passing logic 
becomes more complicated (e.g. double pane logic in this app), tests should 
be written (I would personally go with robolectric tests). 

#### dagger
What can I say - if `Activities` are __GOD__ objects than what is Dagger...  
The Approach here is to use 3 scopes:  
`@Singleton` - well... single per app  
`@ScreenScope` - instance with this scope persist config changes on single screen  
unscoped - the rest

### Platform and Design
One of the important answers I was trying to find with my proposed MVP is 
how to reuse presenters as building blocks for different screen scenarios.

![landscape](readme/landscape.png)

As was shown here this approach works fine with e.g. master detail flow, and 
double panes can be achieved without use of android architecture components or
fragments (which I'm not huge fan of...).

![portrait](readme/portrait.png)

Even rotating screens on devices that vertically have single pane, but horizontally 
two - works fine.

In terms of design, usage of themes and resources in the app, much could 
be polished - but that's something I'm not capable of doing without some 
colleague designer sitting next to me ;)

## Next steps
Even with such a small application development, being _one man team_ usually leads 
to mistakes, bad practices and shortcuts... 
The code proposed here might have flaws that I'm not aware of... 
so please be my reviewer and let's discuss them :)
