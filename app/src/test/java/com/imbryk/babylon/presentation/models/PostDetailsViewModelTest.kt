package com.imbryk.babylon.presentation.models

import android.view.View
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class PostDetailsViewModelTest {
    private val tested = PostDetailsViewModel()

    @Test
    fun `should be created with default values`() {
        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.detailsVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.noContentVisibility.get()).isEqualTo(View.VISIBLE)
    }

    @Test
    fun `should display loader when loading`() {
        tested.isLoadingData.set(true)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.VISIBLE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.detailsVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.noContentVisibility.get()).isEqualTo(View.GONE)
    }

    @Test
    fun `should display error when received error`() {
        tested.errorLoadingDetails.set(true)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.VISIBLE)
        assertThat(tested.detailsVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.noContentVisibility.get()).isEqualTo(View.GONE)
    }

    @Test
    fun `should display list when loading is finished`() {
        tested.isLoadingData.set(true)
        tested.isLoadingData.set(false)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.detailsVisibility.get()).isEqualTo(View.VISIBLE)
        assertThat(tested.noContentVisibility.get()).isEqualTo(View.GONE)
    }
}
