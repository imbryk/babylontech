package com.imbryk.babylon.presentation

import android.view.View
import com.imbryk.babylon.domain.GetPostsUseCase
import com.imbryk.babylon.domain.Post
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import com.imbryk.babylon.presentation.models.PostsListViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers.trampoline
import io.reactivex.subjects.PublishSubject
import org.junit.Test

class PostsListPresenterTest {

    private val postsSubject: PublishSubject<List<Post>> = PublishSubject.create()
    private val getPostsUseCase: GetPostsUseCase = mock {
        on { getPosts() } doReturn postsSubject.firstOrError()
    }
    private val viewModel: PostsListViewModel = mock {
        on { isLoadingData } doReturn mock()
        on { errorLoadData } doReturn mock()
        on { posts } doReturn mock()
    }
    private val schedulers = Schedulers(trampoline(), trampoline())
    private val host: PostsListPresenter.Host = mock()
    private val tested = PostsListPresenter(schedulers, getPostsUseCase)

    @Test
    fun `should start loading list of posts once view is set`() {
        verify(getPostsUseCase, never()).getPosts()

        tested.setView(viewModel)

        verify(getPostsUseCase, times(1)).getPosts()
    }

    @Test
    fun `should show loader while fetching list of posts and hide once data is loaded`() {
        tested.setView(viewModel)

        verify(viewModel.isLoadingData).set(true)

        postsSubject.onNext(emptyList())

        verify(viewModel.isLoadingData).set(false)
    }

    @Test
    fun `should display an error if can't fetch posts`() {
        tested.setView(viewModel)

        postsSubject.onError(Exception())

        verify(viewModel.isLoadingData).set(false)
        verify(viewModel.errorLoadData).set(true)
    }

    @Test
    fun `should display mapped data`() {
        tested.setView(viewModel)

        postsSubject.onNext(
            listOf(
                Post(id = 1, authorName = "user one", authorEmail = "u1@mail.com", title = "first"),
                Post(id = 2, authorName = "user two", authorEmail = "u2@mail.com", title = "second"),
                Post(id = 3, authorName = "user one", authorEmail = "u1@mail.com", title = "third")
            )
        )

        verify(viewModel.posts).set(
            listOf(
                PostListItemViewModel(id = "1", authorName = "user one", imageId = "u1@mail.com", title = "first"),
                PostListItemViewModel(id = "2", authorName = "user two", imageId = "u2@mail.com", title = "second"),
                PostListItemViewModel(id = "3", authorName = "user one", imageId = "u1@mail.com", title = "third")
            )
        )
    }

    @Test
    fun `should notify host when item was selected`() {
        tested.setView(viewModel)
        tested.host = host
        val view: View = mock()
        val item = PostListItemViewModel(id = "3", authorName = "user one", imageId = "u1@mail.com", title = "third")
        tested.onItemClick(item, view)

        verify(host, times(1)).onItemSelected(item, view)
    }
}
