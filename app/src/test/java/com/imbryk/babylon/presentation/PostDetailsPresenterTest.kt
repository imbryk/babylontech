package com.imbryk.babylon.presentation

import com.imbryk.babylon.domain.Comment
import com.imbryk.babylon.domain.GetPostDetailsUseCase
import com.imbryk.babylon.domain.PostDetails
import com.imbryk.babylon.presentation.models.CommentViewModel
import com.imbryk.babylon.presentation.models.PostDetailsViewModel
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.schedulers.Schedulers.trampoline
import io.reactivex.subjects.PublishSubject
import org.junit.Test

class PostDetailsPresenterTest {

    private val postsSubject: PublishSubject<PostDetails> = PublishSubject.create()
    private val getPostDetailsUseCase: GetPostDetailsUseCase = mock {
        on { getPostDetails("1") } doReturn postsSubject.firstOrError()
    }
    private val viewModel: PostDetailsViewModel = mock {
        on { imageId } doReturn mock()
        on { title } doReturn mock()
        on { authorName } doReturn mock()
        on { body } doReturn mock()
        on { isLoadingData } doReturn mock()
        on { errorLoadingDetails } doReturn mock()
        on { commentsCount } doReturn mock()
        on { comments } doReturn mock()
    }
    private val schedulers = Schedulers(
        trampoline(),
        trampoline()
    )
    private val host: PostDetailsPresenter.Host = mock()
    private val tested = PostDetailsPresenter(schedulers, getPostDetailsUseCase)

    @Test
    fun `should display basic posts data once host is set for the first time`() {
        simulatePostDataInHost()
        tested.setView(viewModel)
        tested.host = host

        verify(viewModel.imageId).set("user@email.com")
        verify(viewModel.title).set("some post")
        verify(viewModel.authorName).set("post author")
        verify(viewModel.body, never()).set(any())
    }

    @Test
    fun `should not display basic posts data second time when new host is set`() {
        simulatePostDataInHost()
        tested.setView(viewModel)
        tested.host = host

        verify(viewModel.imageId, times(1)).set("user@email.com")
        verify(viewModel.title, times(1)).set("some post")
        verify(viewModel.authorName, times(1)).set("post author")
        verify(viewModel.body, never()).set(any())
    }

    @Test
    fun `should download post details`() {
        simulatePostDataInHost()

        verify(getPostDetailsUseCase, never()).getPostDetails(any())

        tested.setView(viewModel)
        tested.host = host

        verify(getPostDetailsUseCase, times(1)).getPostDetails("1")
    }

    @Test
    fun `should not download post details second time when new host is set`() {
        simulatePostDataInHost()

        verify(getPostDetailsUseCase, never()).getPostDetails(any())

        tested.setView(viewModel)
        tested.host = host

        tested.host = host

        verify(getPostDetailsUseCase, times(1)).getPostDetails("1")
    }

    @Test
    fun `should show loader while fetching post details, and hide once data is loaded`() {
        simulatePostDataInHost()
        tested.setView(viewModel)
        tested.host = host

        verify(viewModel.isLoadingData).set(true)

        postsSubject.onNext(resultPostDetails())

        verify(viewModel.isLoadingData).set(false)
    }

    @Test
    fun `should display an error if can't fetch post details`() {
        simulatePostDataInHost()
        tested.setView(viewModel)
        tested.host = host

        postsSubject.onError(Exception())

        verify(viewModel.isLoadingData).set(false)
        verify(viewModel.errorLoadingDetails).set(true)
    }

    @Test
    fun `should display post data when it's fetched`() {
        simulatePostDataInHost()
        tested.setView(viewModel)
        tested.host = host

        postsSubject.onNext(resultPostDetails())

        //basic data is loaded second time when full data is loaded
        verify(viewModel.title, times(2)).set("some post")
        verify(viewModel.authorName, times(2)).set("post author")
        verify(viewModel.imageId, times(2)).set("user@email.com")

        verify(viewModel.body).set("post body")
        verify(viewModel.commentsCount).set("2")
        verify(viewModel.comments).set(
            listOf(
                CommentViewModel(name = "comment 1", email = "c1@email.com", body = "comment 1 body"),
                CommentViewModel(name = "comment 2", email = "c2@email.com", body = "comment 2 body")
            )
        )
    }

    private fun resultPostDetails() = PostDetails(
        id = 1,
        title = "some post",
        authorName = "post author",
        body = "post body",
        authorEmail = "user@email.com",
        comments = listOf(
            Comment(name = "comment 1", email = "c1@email.com", body = "comment 1 body"),
            Comment(name = "comment 2", email = "c2@email.com", body = "comment 2 body")
        )
    )

    private fun simulatePostDataInHost() {
        whenever(host.getPostListItem())
            .thenReturn(
                PostListItemViewModel(
                    id = "1",
                    title = "some post",
                    authorName = "post author",
                    imageId = "user@email.com"
                )
            )
    }
}
