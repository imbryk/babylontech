package com.imbryk.babylon.presentation.models

import android.view.View
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class PostsListViewModelTest {
    private val tested = PostsListViewModel()

    @Test
    fun `should be created with default values`() {
        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.listVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.posts.get()).isEqualTo(emptyList<PostListItemViewModel>())
    }

    @Test
    fun `should display loader when loading`() {
        tested.isLoadingData.set(true)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.VISIBLE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.listVisibility.get()).isEqualTo(View.GONE)
    }

    @Test
    fun `should display error when received error`() {
        tested.errorLoadData.set(true)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.VISIBLE)
        assertThat(tested.listVisibility.get()).isEqualTo(View.GONE)
    }

    @Test
    fun `should display list when loading is finished`() {
        tested.isLoadingData.set(true)
        tested.isLoadingData.set(false)

        assertThat(tested.loaderVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.errorVisibility.get()).isEqualTo(View.GONE)
        assertThat(tested.listVisibility.get()).isEqualTo(View.VISIBLE)
    }
}
