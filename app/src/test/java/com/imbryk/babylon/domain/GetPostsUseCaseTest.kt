package com.imbryk.babylon.domain

import com.imbryk.babylon.network.Api
import com.imbryk.babylon.network.PostDto
import com.imbryk.babylon.network.UserDto
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GetPostsUseCaseTest {
    private val api: Api = mock()
    private val tested = GetPostsUseCase(api)

    @Test
    fun `should combine posts with users`() {
        simulatePostsCanBeLoaded()
        simulateUsersCanBeLoaded()

        val testObserver = tested.getPosts().test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]


        assertThat(actualResult).containsExactly(
            Post(id = 1, authorName = "user one", authorEmail = "u1@mail.com", title = "first"),
            Post(id = 2, authorName = "user two", authorEmail = "u2@mail.com", title = "second"),
            Post(id = 3, authorName = "user one", authorEmail = "u1@mail.com", title = "third"),
            Post(id = 4, title = "fourth")
        )
    }

    @Test
    fun `should return posts without authors if users can't be fetched`() {
        simulatePostsCanBeLoaded()
        simulateUsersCanNotBeLoaded()

        val testObserver = tested.getPosts().test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]

        assertThat(actualResult).containsExactly(
            Post(id = 1, title = "first"),
            Post(id = 2, title = "second"),
            Post(id = 3, title = "third"),
            Post(id = 4, title = "fourth")
        )
    }

    @Test
    fun `should return error if posts can't be loaded`() {
        simulatePostsCanNotBeLoaded()
        simulateUsersCanBeLoaded()

        val testObserver = tested.getPosts().test()
        testObserver.assertError(Exception::class.java)
    }

    private fun simulatePostsCanBeLoaded() {
        whenever(api.getPosts()).thenReturn(
            Single.just(
                listOf(
                    PostDto(id = 1, userId = 1, title = "first", body = "first post"),
                    PostDto(id = 2, userId = 2, title = "second", body = "second post"),
                    PostDto(id = 3, userId = 1, title = "third", body = "third post"),
                    PostDto(id = 4, userId = 100, title = "fourth", body = "fourth post")
                )
            )
        )
    }

    private fun simulateUsersCanBeLoaded() {
        whenever(api.getUsers()).thenReturn(
            Single.just(
                listOf(
                    UserDto(id = 1, name = "user one", username = "u1", email = "u1@mail.com"),
                    UserDto(id = 2, name = "user two", username = "u2", email = "u2@mail.com")
                )
            )
        )
    }

    private fun simulatePostsCanNotBeLoaded() {
        whenever(api.getPosts()).thenReturn(
            Single.error(Exception())
        )
    }

    private fun simulateUsersCanNotBeLoaded() {
        whenever(api.getUsers()).thenReturn(
            Single.error(Exception())
        )
    }

}
