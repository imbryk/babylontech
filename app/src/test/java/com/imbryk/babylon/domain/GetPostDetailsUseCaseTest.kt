package com.imbryk.babylon.domain

import com.imbryk.babylon.network.Api
import com.imbryk.babylon.network.CommentDto
import com.imbryk.babylon.network.PostDto
import com.imbryk.babylon.network.UserDto
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GetPostDetailsUseCaseTest {

    private val api: Api = mock()
    private val tested = GetPostDetailsUseCase(api)

    private val testedPostId = "3"

    @Test
    fun `should combine post with user and comments`() {
        simulatePostCanBeLoaded()
        simulateUserCanBeLoaded()
        simulateCommentsCanBeLoaded()

        val testObserver = tested.getPostDetails(testedPostId).test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]

        assertThat(actualResult).isEqualToComparingFieldByFieldRecursively(
            PostDetails(
                id = 3,
                title = "post title",
                body = "post body",
                authorName = "some user",
                authorEmail = "u2@mail.com",
                comments = listOf(
                    Comment(name = "comment 2", body = "comment 2 body", email = "c1@mail.com"),
                    Comment(name = "comment 4", body = "comment 4 body", email = "c2@mail.com"),
                    Comment(name = "comment 6", body = "comment 6 body", email = "c3@mail.com")
                )
            )
        )
    }

    @Test
    fun `should return post without author if user can't be fetched`() {
        simulatePostCanBeLoaded()
        simulateUserCanNotBeLoaded()
        simulateCommentsCanBeLoaded()

        val testObserver = tested.getPostDetails(testedPostId).test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]

        assertThat(actualResult).isEqualToComparingFieldByFieldRecursively(
            PostDetails(
                id = 3,
                title = "post title",
                body = "post body",
                authorName = null,
                authorEmail = null,
                comments = listOf(
                    Comment(name = "comment 2", body = "comment 2 body", email = "c1@mail.com"),
                    Comment(name = "comment 4", body = "comment 4 body", email = "c2@mail.com"),
                    Comment(name = "comment 6", body = "comment 6 body", email = "c3@mail.com")
                )
            )
        )
    }

    @Test
    fun `should return post without comments if they can't be fetched`() {
        simulatePostCanBeLoaded()
        simulateUserCanBeLoaded()
        simulateCommentsCanNotBeLoaded()

        val testObserver = tested.getPostDetails(testedPostId).test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]

        assertThat(actualResult).isEqualToComparingFieldByFieldRecursively(
            PostDetails(
                id = 3,
                title = "post title",
                body = "post body",
                authorName = "some user",
                authorEmail = "u2@mail.com",
                comments = emptyList()
            )
        )
    }

    @Test
    fun `should return post without comments and author if they can't be fetched`() {
        simulatePostCanBeLoaded()
        simulateUserCanNotBeLoaded()
        simulateCommentsCanNotBeLoaded()

        val testObserver = tested.getPostDetails(testedPostId).test()
        testObserver.awaitCount(1)
        val actualResult = testObserver.values()[0]

        assertThat(actualResult).isEqualToComparingFieldByFieldRecursively(
            PostDetails(
                id = 3,
                title = "post title",
                body = "post body",
                authorName = null,
                authorEmail = null,
                comments = emptyList()
            )
        )
    }

    @Test
    fun `should return error if post can't be loaded`() {
        simulatePostCanNotBeLoaded()
        simulateUserCanBeLoaded()
        simulateCommentsCanBeLoaded()

        val testObserver = tested.getPostDetails(testedPostId).test()
        testObserver.assertError(Exception::class.java)
    }

    private fun simulateCommentsCanBeLoaded() {
        whenever(api.getComments(testedPostId)).thenReturn(
            Single.just(
                listOf(
                    CommentDto(id = 2, name = "comment 2", body = "comment 2 body", postId = 3, email = "c1@mail.com"),
                    CommentDto(id = 4, name = "comment 4", body = "comment 4 body", postId = 3, email = "c2@mail.com"),
                    CommentDto(id = 6, name = "comment 6", body = "comment 6 body", postId = 3, email = "c3@mail.com")
                )
            )
        )
    }

    private fun simulateCommentsCanNotBeLoaded() {
        whenever(api.getComments(any())).thenReturn(
            Single.error(Exception())
        )
    }

    private fun simulateUserCanBeLoaded() {
        whenever(api.getUser("2")).thenReturn(
            Single.just(
                UserDto(id = 2, name = "some user", username = "u2", email = "u2@mail.com")
            )
        )
    }

    private fun simulateUserCanNotBeLoaded() {
        whenever(api.getUser(any())).thenReturn(
            Single.error(Exception())
        )
    }

    private fun simulatePostCanBeLoaded() {
        whenever(api.getPost(testedPostId)).thenReturn(
            Single.just(
                PostDto(id = 3, userId = 2, title = "post title", body = "post body")
            )
        )
    }

    private fun simulatePostCanNotBeLoaded() {
        whenever(api.getPost(testedPostId)).thenReturn(
            Single.error(Exception())
        )

    }

}
