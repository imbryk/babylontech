package com.imbryk.babylon.presentation.models

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.view.View
import com.imbryk.babylon.presentation.ViewModel
import pl.xsolve.mvp.dagger.scope.ScreenScope
import javax.inject.Inject

@ScreenScope
class PostsListViewModel @Inject constructor() : ViewModel<PostsListViewModel>() {
    val isLoadingData = ObservableBoolean(false)
    val errorLoadData = ObservableBoolean(false)

    val listVisibility = ObservableInt(View.GONE)
    val loaderVisibility = ObservableInt(View.GONE)
    val errorVisibility = ObservableInt(View.GONE)

    val posts = ObservableField<List<PostListItemViewModel>>(emptyList())

    init {
        val propertyCallback = PropertyCallback()
        isLoadingData.addOnPropertyChangedCallback(propertyCallback)
        errorLoadData.addOnPropertyChangedCallback(propertyCallback)
    }

    inner class PropertyCallback : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            listVisibility.set(toViewVisibility(!isLoadingData.get() && !errorLoadData.get()))
            when (sender) {
                isLoadingData -> loaderVisibility.set(toViewVisibility(isLoadingData.get()))
                errorLoadData -> errorVisibility.set(toViewVisibility(errorLoadData.get()))
            }
        }
    }
}
