package com.imbryk.babylon.presentation

import com.imbryk.babylon.domain.GetPostDetailsUseCase
import com.imbryk.babylon.domain.PostDetails
import com.imbryk.babylon.presentation.models.CommentViewModel
import com.imbryk.babylon.presentation.models.PostDetailsViewModel
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import io.reactivex.disposables.Disposable
import pl.xsolve.mvp.Presenter
import pl.xsolve.mvp.dagger.scope.ScreenScope
import javax.inject.Inject

@ScreenScope
class PostDetailsPresenter @Inject constructor(
    private val schedulers: Schedulers,
    private val postDetailsUseCase: GetPostDetailsUseCase
) : Presenter<PostDetailsViewModel>() {

    private var subscription: Disposable? = null

    var host: Host? = null
        set(value) {
            val firstHostSet = field == null
            field = value
            if (firstHostSet) {
                loadPostDetails()
            }
        }

    fun loadPostDetails() {
        val item = host?.getPostListItem()
        item?.let {
            displayAvailablePostDetails(it)
            downloadPostDetails(it.id)
        }
    }

    private fun displayAvailablePostDetails(item: PostListItemViewModel) {
        view.authorName.set(item.authorName)
        view.title.set(item.title)
        view.imageId.set(item.imageId)
    }

    private fun downloadPostDetails(postId: String) {
        view.isLoadingData.set(true)
        subscription?.dispose()
        subscription = postDetailsUseCase
            .getPostDetails(postId)
            .usingSchedulers(schedulers)
            .subscribe(this::postDetailsReceived, this::errorLoadingPosts)
    }

    private fun postDetailsReceived(post: PostDetails) {
        view.isLoadingData.set(false)
        view.authorName.set(post.authorName)
        view.title.set(post.title)
        view.imageId.set(post.authorEmail)
        view.body.set(post.body)
        view.commentsCount.set(post.comments.size.toString())
        view.comments.set(post.comments.map { comment ->
            CommentViewModel(
                name = comment.name,
                body = comment.body,
                email = comment.email
            )
        })
    }

    private fun errorLoadingPosts(error: Throwable) {
        view.isLoadingData.set(false)
        view.errorLoadingDetails.set(true)
    }

    override fun onFinish() {
        subscription?.dispose()
    }

    interface Host {
        fun getPostListItem(): PostListItemViewModel?
    }
}
