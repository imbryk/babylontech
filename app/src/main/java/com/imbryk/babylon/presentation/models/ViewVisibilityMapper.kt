package com.imbryk.babylon.presentation.models

import android.view.View

fun toViewVisibility(visible: Boolean, whenNotVisible: Int = View.GONE) =
    if (visible) View.VISIBLE else whenNotVisible
