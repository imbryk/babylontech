package com.imbryk.babylon.presentation

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.io as ioScheduler

class Schedulers constructor(
    val io: Scheduler = ioScheduler(),
    val mainThread: Scheduler = AndroidSchedulers.mainThread()
)

fun <T> Single<T>.usingSchedulers(schedulers: Schedulers): Single<T> =
    this.subscribeOn(schedulers.io).observeOn(schedulers.mainThread)
