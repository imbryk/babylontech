package com.imbryk.babylon.presentation.models

import android.os.Parcelable
import com.imbryk.babylon.R
import com.imbryk.babylon.android.adapters.BindingData
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostListItemViewModel(
    val id: String,
    val title: String,
    val authorName: String?,
    val imageId: String?
) : BindingData, Parcelable {

    @IgnoredOnParcel
    override val layoutId: Int = R.layout.post_list_item
}
