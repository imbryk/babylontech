package com.imbryk.babylon.presentation

import pl.xsolve.mvp.ViewState

open class ViewModel<T> : ViewState<T>() {
    override fun hasView() = true //hack to mvp library to allow ViewState that is a View
}
