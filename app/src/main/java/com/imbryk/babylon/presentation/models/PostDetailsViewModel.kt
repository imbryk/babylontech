package com.imbryk.babylon.presentation.models

import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.view.View
import com.imbryk.babylon.domain.Comment
import com.imbryk.babylon.presentation.ViewModel
import pl.xsolve.mvp.dagger.scope.ScreenScope
import javax.inject.Inject

@ScreenScope
class PostDetailsViewModel @Inject constructor() : ViewModel<PostDetailsViewModel>() {
    val title = ObservableField<String>()
    val authorName = ObservableField<String>()
    val imageId = ObservableField<String>()
    val body = ObservableField<String>()
    val commentsCount = ObservableField<String>()
    val comments = ObservableField<List<CommentViewModel>>()
    val isLoadingData = ObservableBoolean()
    val errorLoadingDetails = ObservableBoolean()


    val loaderVisibility = ObservableInt(View.GONE)
    val errorVisibility = ObservableInt(View.GONE)
    val detailsVisibility = ObservableInt(View.GONE)

    val noContentVisibility = ObservableInt(View.VISIBLE)

    init {
        val propertyCallback = PropertyCallback()
        isLoadingData.addOnPropertyChangedCallback(propertyCallback)
        errorLoadingDetails.addOnPropertyChangedCallback(propertyCallback)
    }

    inner class PropertyCallback : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            detailsVisibility.set(toViewVisibility(!isLoadingData.get() && !errorLoadingDetails.get()))
            noContentVisibility.set(View.GONE)
            when (sender) {
                isLoadingData -> loaderVisibility.set(toViewVisibility(isLoadingData.get()))
                errorLoadingDetails -> errorVisibility.set(toViewVisibility(errorLoadingDetails.get()))
            }
        }
    }
}
