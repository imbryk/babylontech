package com.imbryk.babylon.presentation

import android.view.View
import com.imbryk.babylon.android.adapters.BindingData
import com.imbryk.babylon.android.adapters.DataClickListener
import com.imbryk.babylon.domain.GetPostsUseCase
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import com.imbryk.babylon.presentation.models.PostsListViewModel
import io.reactivex.disposables.Disposable
import pl.xsolve.mvp.Presenter
import pl.xsolve.mvp.dagger.scope.ScreenScope
import javax.inject.Inject

@ScreenScope
class PostsListPresenter @Inject constructor(
    private val schedulers: Schedulers,
    private val postsUseCase: GetPostsUseCase
) : Presenter<PostsListViewModel>(), DataClickListener {

    var host: Host? = null

    private var subscription: Disposable? = null

    override fun setView(view: PostsListViewModel) {
        super.setView(view)
        loadPosts()
    }

    private fun loadPosts() {
        view.isLoadingData.set(true)
        subscription = postsUseCase
            .getPosts()
            .map { posts ->
                posts.map { post ->
                    PostListItemViewModel(
                        id = post.id.toString(),
                        title = post.title,
                        authorName = post.authorName,
                        imageId = post.authorEmail
                    )
                }
            }
            .usingSchedulers(schedulers)
            .subscribe(this::postsReceived, this::errorLoadingPosts)
    }

    private fun errorLoadingPosts(error: Throwable) {
        view.isLoadingData.set(false)
        view.errorLoadData.set(true)
    }

    private fun postsReceived(posts: List<PostListItemViewModel>) {
        view.isLoadingData.set(false)
        view.posts.set(posts)
    }

    override fun onFinish() {
        subscription?.dispose()
    }

    override fun onItemClick(item: BindingData, view: View) {
        host?.onItemSelected(item as PostListItemViewModel, view)
    }

    interface Host {
        fun onItemSelected(item: PostListItemViewModel, view: View)
    }

}
