package com.imbryk.babylon.presentation.models

import com.imbryk.babylon.R
import com.imbryk.babylon.android.adapters.BindingData

data class CommentViewModel(
    val name: String,
    val email: String,
    val body: String
) : BindingData {
    override val layoutId: Int = R.layout.comment_list_item
}
