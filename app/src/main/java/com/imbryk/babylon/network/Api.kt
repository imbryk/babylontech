package com.imbryk.babylon.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    @GET("posts/{id}")
    fun getPost(@Path("id") id: String): Single<PostDto>

    @GET("posts")
    fun getPosts(): Single<List<PostDto>>

    @GET("users/{id}")
    fun getUser(@Path("id") id: String): Single<UserDto>

    @GET("users")
    fun getUsers(): Single<List<UserDto>>

    @GET("comments")
    fun getComments(@Query("postId") postId: String): Single<List<CommentDto>>

}
