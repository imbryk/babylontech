package com.imbryk.babylon.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Network @Inject constructor() {

    private val retrofit = Retrofit.Builder()
        .baseUrl(ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    fun getApi(): Api = retrofit.create(Api::class.java)

    companion object {
        private const val ENDPOINT = "https://jsonplaceholder.typicode.com"
    }
}
