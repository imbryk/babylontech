package com.imbryk.babylon.network

data class PostDto(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)

data class UserDto(
    val id: Int,
    val name: String,
    val username: String,
    val email: String
)

data class CommentDto(
    val id: Int,
    val postId: Int,
    val name: String,
    val email: String,
    val body: String
)
