package com.imbryk.babylon.android

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil
import com.imbryk.babylon.R
import com.imbryk.babylon.dagger.AppComponentFactory
import com.imbryk.babylon.dagger.DaggerMainComponent
import com.imbryk.babylon.dagger.MainComponent
import com.imbryk.babylon.databinding.ActivityDetailsBinding
import com.imbryk.babylon.presentation.PostDetailsPresenter
import com.imbryk.babylon.presentation.models.PostDetailsViewModel
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import pl.xsolve.mvp.api.MvpPresenter
import pl.xsolve.mvp.api.MvpViewState
import pl.xsolve.mvp.dagger.MvpActivityComponent
import javax.inject.Inject

class DetailActivity : BaseActivity(), PostDetailsPresenter.Host {

    @Inject
    @MvpPresenter(viewState = "postDetailsViewModel")
    lateinit var postDetailsPresenter: PostDetailsPresenter

    @Inject
    @MvpViewState
    lateinit var postDetailsViewModel: PostDetailsViewModel

    override fun createComponent(): MainComponent {
        return DaggerMainComponent
            .builder()
            .appComponent(AppComponentFactory.get())
            .build()
    }

    override fun inject(component: MvpActivityComponent) {
        (component as MainComponent).inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        postDetailsPresenter.host = this

        val binding: ActivityDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.postDetails = postDetailsViewModel

        val doublePane = binding.postDetailsLayout == null
        if (doublePane) {
            finish()
            return
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun getPostListItem(): PostListItemViewModel = intent.getParcelableExtra(ARG_POST)


    companion object {
        private const val ARG_POST = "DetailActivity.ARG_POST"

        fun launch(activity: Activity, post: PostListItemViewModel, view: View) {
            val intent = Intent(activity, DetailActivity::class.java)
            intent.putExtra(ARG_POST, post)
            val options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(activity,
                    Pair.create(view.findViewById(R.id.avatar), "avatar")
            )
            activity.startActivity(intent, options.toBundle())
        }
    }
}
