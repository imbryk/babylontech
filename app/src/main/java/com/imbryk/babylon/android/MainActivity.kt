package com.imbryk.babylon.android

import android.os.Bundle
import android.transition.Fade
import android.view.View
import androidx.databinding.DataBindingUtil
import com.imbryk.babylon.R
import com.imbryk.babylon.dagger.AppComponentFactory
import com.imbryk.babylon.dagger.DaggerMainComponent
import com.imbryk.babylon.dagger.MainComponent
import com.imbryk.babylon.databinding.ActivityMainBinding
import com.imbryk.babylon.presentation.PostDetailsPresenter
import com.imbryk.babylon.presentation.PostsListPresenter
import com.imbryk.babylon.presentation.models.PostDetailsViewModel
import com.imbryk.babylon.presentation.models.PostListItemViewModel
import com.imbryk.babylon.presentation.models.PostsListViewModel
import pl.xsolve.mvp.api.MvpPresenter
import pl.xsolve.mvp.api.MvpViewState
import pl.xsolve.mvp.dagger.MvpActivityComponent
import javax.inject.Inject


class MainActivity : BaseActivity(), PostsListPresenter.Host, PostDetailsPresenter.Host {

    @Inject
    @MvpPresenter(viewState = "postsListViewModel")
    lateinit var postsListPresenter: PostsListPresenter

    @Inject
    @MvpViewState
    lateinit var postsListViewModel: PostsListViewModel

    @Inject
    @MvpPresenter(viewState = "postDetailsViewModel")
    lateinit var postDetailsPresenter: PostDetailsPresenter

    @Inject
    @MvpViewState
    lateinit var postDetailsViewModel: PostDetailsViewModel

    private var doublePane = false

    override fun createComponent(): MainComponent {
        return DaggerMainComponent
            .builder()
            .appComponent(AppComponentFactory.get())
            .build()
    }

    override fun inject(component: MvpActivityComponent) {
        (component as MainComponent).inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postsListPresenter.host = this
        postDetailsPresenter.host = this

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.presenter = postsListPresenter
        binding.view = postsListViewModel
        binding.postDetails = postDetailsViewModel

        doublePane = binding.postDetailsLayout != null
        if (doublePane) {
            postDetailsPresenter.loadPostDetails()
        }
    }

    override fun onItemSelected(item: PostListItemViewModel, view: View) {
        intent.putExtra(ARG_POST, item)
        if (doublePane) {
            postDetailsPresenter.loadPostDetails()
        } else {
            DetailActivity.launch(this, item, view)
        }
    }

    override fun getPostListItem(): PostListItemViewModel? = intent.getParcelableExtra(ARG_POST)

    companion object {
        private const val ARG_POST = "MainActivity.ARG_POST"
    }
}
