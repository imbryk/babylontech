package com.imbryk.babylon.android

import android.app.Application
import com.imbryk.babylon.dagger.AppComponentFactory

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        AppComponentFactory.create(this)
    }
}
