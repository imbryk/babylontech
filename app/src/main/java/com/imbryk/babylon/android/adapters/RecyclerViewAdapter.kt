package com.imbryk.babylon.android.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.imbryk.babylon.BR

class RecyclerViewAdapter :
    RecyclerView.Adapter<RecyclerViewHolder>(),
    DataClickListener {

    var data: List<BindingData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var clickListener: DataClickListener? = null

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ViewDataBinding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return RecyclerViewHolder(itemBinding, this)
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].layoutId
    }

    override fun getItemCount() = data.size

    override fun onItemClick(item: BindingData, view: View) {
        clickListener?.onItemClick(item, view)
    }
}

interface BindingData {
    val layoutId: Int
}

interface DataClickListener {
    fun onItemClick(item: BindingData, view: View)
}

class RecyclerViewHolder(
    private val binding: ViewDataBinding,
    private val clickListener: DataClickListener
) : RecyclerView.ViewHolder(binding.root),
    View.OnClickListener {

    private lateinit var item: BindingData

    override fun onClick(view: View) {
        clickListener.onItemClick(item, view)
    }

    fun bind(item: BindingData) {
        this.item = item
        binding.setVariable(BR.itemData, item)
        binding.setVariable(BR.itemClickListener, this)
        binding.executePendingBindings()
    }
}
