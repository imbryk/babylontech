package com.imbryk.babylon.android

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.imbryk.babylon.R
import pl.xsolve.mvp.MvpActivity

open class BaseActivity : MvpActivity() {

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        window.enterTransition?.excludeTarget(android.R.id.statusBarBackground, true)
        window.enterTransition?.excludeTarget(android.R.id.navigationBarBackground, true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
