package com.imbryk.babylon.android.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

object RecyclerViewBindingAdapter {

    @JvmStatic
    @BindingAdapter("dataClickListener")
    fun setClickListener(recyclerView: RecyclerView, listener: DataClickListener?) {
        recyclerView.assertAdapter().clickListener = listener
    }

    @JvmStatic
    @BindingAdapter("data")
    fun loadData(recyclerView: RecyclerView, data: List<BindingData>?) {
        recyclerView.assertAdapter().data = data ?: emptyList()
    }

    private fun RecyclerView.assertAdapter(): RecyclerViewAdapter {
        if (!this.hasCorrectAdapter()) {
            this.adapter = RecyclerViewAdapter()
        }
        return this.adapter as RecyclerViewAdapter
    }

    private fun RecyclerView.hasCorrectAdapter() =
        this.adapter != null && this.adapter is RecyclerViewAdapter
}


