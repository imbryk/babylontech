package com.imbryk.babylon.android.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.imbryk.babylon.R

object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("adorableImage")
    fun loadAdorableImage(imageView: ImageView, url: String?) {
        if (url.orEmpty().isNotEmpty()) {
            Glide
                .with(imageView.context)
                .load(BASE_URL + url)
                .centerCrop()
                .fallback(R.drawable.adorable)
                .into(imageView)
        }
    }

    private const val BASE_URL = "https://api.adorable.io/avatars/256/"
}
