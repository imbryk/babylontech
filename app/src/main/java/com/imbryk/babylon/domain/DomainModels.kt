package com.imbryk.babylon.domain

data class Post(
    val id: Int,
    val title: String,
    val authorName: String? = null,
    val authorEmail: String? = null
)

data class PostDetails(
    val id: Int,
    val title: String,
    val body: String,
    val comments: List<Comment> = emptyList(),
    val authorName: String? = null,
    val authorEmail: String? = null
)

data class Comment(
    val name: String,
    val email: String,
    val body: String

)
