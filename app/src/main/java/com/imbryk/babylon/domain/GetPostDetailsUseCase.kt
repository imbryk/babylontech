package com.imbryk.babylon.domain

import com.imbryk.babylon.network.Api
import com.imbryk.babylon.network.CommentDto
import com.imbryk.babylon.network.UserDto
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetPostDetailsUseCase @Inject constructor(val api: Api) {
    fun getPostDetails(postId: String): Single<PostDetails> {
        return api.getPost(postId)
            .flatMap { postDto ->
                Single.zip(
                    api.getUser(postDto.userId.toString()).onErrorReturn { emptyUserDto },
                    api.getComments(postId).onErrorReturn { emptyList() },
                    BiFunction { userDto: UserDto, commentsDto: List<CommentDto> ->
                        PostDetails(
                            id = postDto.id,
                            title = postDto.title,
                            body = postDto.body,
                            authorName = userDto.nullIfEmpty()?.name,
                            authorEmail = userDto.nullIfEmpty()?.email,
                            comments = commentsDto.map { commentDto ->
                                Comment(
                                    name = commentDto.name,
                                    body = commentDto.body,
                                    email = commentDto.email
                                )
                            }
                        )
                    }
                )
            }
    }

    private val emptyUserDto = UserDto(-1, "", "", "")
    private fun UserDto.nullIfEmpty(): UserDto? = if (this == emptyUserDto) null else this
}
