package com.imbryk.babylon.domain

import com.imbryk.babylon.network.Api
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetPostsUseCase @Inject constructor(val api: Api) {
    fun getPosts(): Single<List<Post>> {
        return Single.zip(
            api.getPosts(),
            api.getUsers().onErrorReturn { emptyList() },
            BiFunction { posts, users ->
                posts.map { postDto ->
                    val userDto = users.find { it.id == postDto.userId }
                    Post(
                        id = postDto.id,
                        title = postDto.title,
                        authorName = userDto?.name,
                        authorEmail = userDto?.email
                    )
                }
            })
    }
}
