package com.imbryk.babylon.dagger

import android.app.Application
import pl.xsolve.mvp.dagger.MvpAppModule

class AppComponentFactory {
    companion object {

        private var appComponent: AppComponent? = null

        fun create(app: Application) {
            appComponent?.let {
                throw RuntimeException("AppComponent already created")
            }

            appComponent = DaggerAppComponent
                .builder()
                .mvpAppModule(MvpAppModule(app.applicationContext))
                .build()
        }

        fun get(): AppComponent {
            appComponent?.let {
                return it
            } ?: throw RuntimeException("call create() from application before calling get()")
        }
    }
}
