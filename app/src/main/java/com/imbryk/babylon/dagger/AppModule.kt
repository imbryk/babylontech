package com.imbryk.babylon.dagger

import com.imbryk.babylon.network.Api
import com.imbryk.babylon.network.Network
import com.imbryk.babylon.presentation.Schedulers
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApi(network: Network): Api {
        return network.getApi()
    }

    @Provides
    @Singleton
    fun provideSchedulers(): Schedulers {
        return Schedulers()
    }
}
