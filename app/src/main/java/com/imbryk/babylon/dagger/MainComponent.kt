package com.imbryk.babylon.dagger

import com.imbryk.babylon.android.DetailActivity
import com.imbryk.babylon.android.MainActivity
import dagger.Component
import pl.xsolve.mvp.dagger.MvpActivityComponent
import pl.xsolve.mvp.dagger.MvpActivityModule
import pl.xsolve.mvp.dagger.scope.ScreenScope

@ScreenScope
@Component(
    dependencies = [
        AppComponent::class
    ],
    modules = [
        MvpActivityModule::class
    ]
)
interface MainComponent : MvpActivityComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: DetailActivity)
}
