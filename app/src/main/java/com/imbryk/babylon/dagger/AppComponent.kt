package com.imbryk.babylon.dagger

import android.content.Context
import com.imbryk.babylon.domain.GetPostDetailsUseCase
import com.imbryk.babylon.domain.GetPostsUseCase
import com.imbryk.babylon.network.Api
import com.imbryk.babylon.presentation.Schedulers
import dagger.Component
import pl.xsolve.mvp.dagger.MvpAppModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        MvpAppModule::class,
        AppModule::class
    ]
)
interface AppComponent {
    fun appContext(): Context
    fun api(): Api
    fun getPostsUseCase(): GetPostsUseCase
    fun getGetPostDetailsUseCase(): GetPostDetailsUseCase
    fun schedulers(): Schedulers
}
